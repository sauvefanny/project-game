import { Puissance4 } from "./classes/Puissance4";
import { Player } from "./classes/Player";


let player1 = new Player(1, 'Jean');
let player2 = new Player(2, 'Catherine');

function startNewGame(player1, player2) {
    let p4 = new Puissance4(6, 7);
    p4.start([player1, player2]);

    return p4;
}

let game = startNewGame(player1, player2);

let resetButton = document.querySelector('#reset');
resetButton.addEventListener('click', () => {
    game.reset();
    game = startNewGame(player1, player2);
});
