export class Puissance4 {
    cols;
    rows;
    cells;
    players;
    currentPlayer;

    constructor(pararows, paracols) {
        this.rows = pararows;
        this.cols = paracols;
        this.cells = [];

        for (let y = 1; y <= this.cols; y++) {
            let cols = [];
            this.cells[y] = cols;
            for (let x = 1; x <= this.rows; x++) {
                cols.push(null);
            }
        }

    }

    toHTML() {
        let container = document.querySelector('#game');

        let playerTurn = document.createElement('div');
        playerTurn.classList.add('playerTurn');
        container.appendChild(playerTurn);

        let board = document.createElement('ul');
        board.classList.add('board');

        for (let x = 1; x <= this.cols; x++) {
            let col = document.createElement('li');
            let containerRows = document.createElement('ul');
            containerRows.classList.add('col');
            containerRows.setAttribute('data-col', x);
            for (let y = 1; y <= this.rows; y++) {
                let row = document.createElement('li');
                row.setAttribute('data-row', y);
                containerRows.appendChild(row);
            }

            let thisgame = this; // current puissance4 object
            containerRows.addEventListener('click', function () {
                thisgame.play(this); // call thisgame instead of this, because this is event context
            });

            col.appendChild(containerRows);
            board.appendChild(col);

        }
        container.appendChild(board);
    }

    start(players) {
    
        this.players = players;
        this.currentPlayer = this.players[0];
        this.toHTML();

        // set player turn
        this.playerTurn();
    }

    playerTurn() {
        let playerTurn = document.querySelector('.playerTurn');
        playerTurn.innerHTML = "C'est à <span class='player" + this.currentPlayer.id + "'>" + this.currentPlayer.name + '</span> de jouer';
    }

    play(col) {

        // Build class to set the cell color
        let playedClass = 'player' + this.currentPlayer.id;

        // get col number
        let colNb = col.getAttribute('data-col');

        for (let rowNb = this.rows; rowNb >= 1; rowNb--) {
            if (this.cells[colNb][rowNb - 1] === null) { // -1 because this.cell row starts with 0
                this.cells[colNb][rowNb - 1] = this.currentPlayer;
                let boardCell = document.querySelector('#game [data-col="' + colNb + '"] [data-row="' + rowNb + '"]');
                boardCell.classList.add(playedClass);

                if (this.isGameEnded(colNb, rowNb - 1)) {
                    console.log('fin du jeu');
                    this.endGame();
                } else {
                    console.log('le jeu continue');
                    this.switchPlayer();
                    this.playerTurn();
                }
                break;
            }
        }
    }

    switchPlayer() {
        if (this.currentPlayer === this.players[0]) {
            this.currentPlayer = this.players[1];
        } else {
            this.currentPlayer = this.players[0];
        }
    }

    isGameEnded(colNb, rowNb) {
        let playerId = this.currentPlayer.id;

        // vertical victory
        let countY = 0;

        // for each cell in this col
        for (let row = 0; row < this.rows; row++) {
            if (this.cells[colNb][row] !== null && this.cells[colNb][row].id === playerId) {
                countY++;
            } else {
                countY = 0;
            }

            if (countY >= 4) {
                console.log('victoire verticale');
                return true;
            }
        }

        // horizontal victory
        let countX = 0;

        // for each cell in this row
        for (let col = 1; col <= this.cols; col++) {
            let cell = this.cells[col][rowNb];

            if (cell !== null && cell.id === playerId) {
                countX++;
            } else {
                countX = 0;
            }

            if (countX >= 4) {
                console.log('victoire horizontale');
                return true;
            }
        }

        return false;
    }

    endGame() {
        let playerTurn = document.querySelector('.playerTurn');
        playerTurn.innerHTML = "Victoire de <span class='player" + this.currentPlayer.id + "'>" + this.currentPlayer.name + '</span> !';

    }
    reset() {
        document.querySelector('#game').textContent = '';
    }

}//fin de la class puissance4


