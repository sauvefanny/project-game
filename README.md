# Projet Puissance 4

>L'objectif de ce projet est de faire un petit jeu en Javascript sans framework.
Il va permettre d'utiliser le JS, l'algorithmie de base, la manipulation du DOM et la POO dans un contexte ludique... mais professionnel.
Le type et le thème du jeu sont libres (en sachant qu'il sera sur votre portfolio, donc pas n'importe quoi non plus).


>J'ai choisi de construire a laide de HTML CSS et JAVASCRIPT un puissance 4 ceprojet me semblait interressant pour commencer a penser à la logique algorythmique et utilser les classes.

## Maquettes

J'ai réalisé mes maquettes sur figma: 

<img src="/public/image/Projet-game.png" width="300" height="300" />

Créer un board dans le dépôt gitlab du jeu dans lequel vous listerez toutes les fonctionnalités à coder j'ai créer une milestone un Sprint( cf voir depot gitlab : 
> https://gitlab.com/sauvefanny/project-game )

Pour chaque fonction/méthode, je n'ai pas encore pris l'habitude de faire la doc j'ai cependant commencer a mettre des commentaires.


## Aspects techniques 

J'ai utilisé deux classes Javascript:
1. Player
2. Puissance4

J'ai instancier un plateau de jeux par le biais de mon index.js.
J'ai fait de cette instance une fonction afin de pourvoir l'appelé après l'addEventListener au click.

Ma class Puissance4:
```
export class Puissance4 {
    cols;
    rows;
    cells;
    players;
    currentPlayer;
    gameId;

    constructor(pararows, paracols, gameId) {
        this.rows = pararows;
        this.cols = paracols;
        this.cells = [];

        for (let y = 1; y <= this.cols; y++) {
            let cols = [];
            this.cells[y] = cols;
            for (let x = 1; x <= this.rows; x++) {
                cols.push(null);
            }
        }

    }
    export class Player{
    id;
    name;

    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}
```
Comme vous pouvez le voir, j'ai construis plusieurs objets. Cela m'a permis ensuite, de créer un tableau multi-dimensionelle.\
*En règle générale, hors programmation, un tableau a souvent 2 dimensions. En Javascript, un tableau n’est autre qu’un objet où sont stockées des données.*\
J'ai lié ce tableau avec une méthode toHTML().

La methode start permet de lancer l'instance du jeux et des players.
Ensuite playTurn() est une methode qui va afficher un bandeau en haut de la page html avec le joueur courant...\


Je n'ai mis au sein de mon index.html que un id #game et un button avec l'id #reset.
Ce travaille m'a permis de séparer les données de l'affichage.\
De manière générale j'ai pu mettre en pratique la notion de classe d'objet et meiux saisi la valeur de this.\
Cependant il va me falloir encore enormement pratiquer.\
